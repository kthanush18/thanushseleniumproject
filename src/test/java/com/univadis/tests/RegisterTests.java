package com.univadis.tests;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.univadis.Register;
import com.web.common.BrowserType;

public class RegisterTests extends TestBase {
	
	@BeforeTest
	public void testIntialize() {
		_register = new Register(BrowserType.CHROME);
		_register.navigateToURL();
	}
	
	@Test (testName = "registerToUnivadis", description = "create new user account for univadis")
	public void registerToUnivadis() throws InterruptedException {
		
		//ACT
		_register.selectRegisterButton();
		_register.enterRegisterDetails();
		Thread.sleep(5000);
	}
	
	@AfterTest
	public void testCleanUP() {
		_register.quitBrowser();
	}

}
