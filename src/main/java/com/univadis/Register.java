package com.univadis;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.w3c.dom.html.HTMLSelectElement;

import com.web.common.BrowserType;
import com.web.common.ElementSelectorType;
import com.web.common.WebBrowser;

public class Register {
	
	private WebBrowser _browser;
	public Register(BrowserType browserName) {
		_browser = new WebBrowser(browserName);
	}
	
	public void navigateToURL() {
		_browser.navigateToURL();
	}
	
	public WebElement getRegisterButtonElement() {
		return _browser.getElement("//div [@class = 'user-profile-desktop']//following::a[4]", ElementSelectorType.Xpath);
	}
	
	public void selectRegisterButton() {
		getRegisterButtonElement().click();
	}
	
	public WebElement getEmailTextBoxElement() {
		return _browser.getElement("//input [@name = \"regEmail\"]", ElementSelectorType.Xpath);
	}
	public WebElement getPasswordTextBoxElement() {
		return _browser.getElement("regPassword", ElementSelectorType.Name);
	}
	public WebElement getCountryOfPracticeElement() {
		return _browser.getElement("div.input-group.isHideDropDown", ElementSelectorType.CssSelector);
	}
	public WebElement getCountryOFPracticeTextBoxElement() {
		return _browser.getElement(".multiselect__input", ElementSelectorType.CssSelector);
	}
	public WebElement getCountryListElement() {
		return _browser.getElement("//li [@class = 'multiselect__element'][2]", ElementSelectorType.Xpath);
	}
	public WebElement getProfessionElement() {
		return _browser.getElement("//div [@tabindex = '0']/..", ElementSelectorType.Xpath);
	}
	public WebElement getProfessionListElement() {
		return _browser.getElement("//span [text() = 'Non-Professional']/../..", ElementSelectorType.Xpath);
	}
	public WebElement getAgreeTermsCheckBoxElement() {
		return _browser.getElement("//input [@name = 'agreeTerms']/..", ElementSelectorType.Xpath);
	}
	public WebElement getCreateAccountElement() {
		return _browser.getElement("//button [@class = 'mdscp-button--submit mdscp-button--medium']", ElementSelectorType.Xpath);
	}
	
	public void enterRegisterDetails() throws InterruptedException {
		getEmailTextBoxElement().sendKeys("thanush@ymail.com");
		getPasswordTextBoxElement().sendKeys("password1!");
		getCountryOfPracticeElement().click();
		getCountryOFPracticeTextBoxElement().sendKeys("India");
		getCountryListElement().click();
		getProfessionElement().click();
		getProfessionListElement().click();
		getAgreeTermsCheckBoxElement().click();
		getCreateAccountElement().click();
	}
	
	public void quitBrowser() {
		_browser.closeBrowser();
		_browser.quitBrowser();
	}
	
}
