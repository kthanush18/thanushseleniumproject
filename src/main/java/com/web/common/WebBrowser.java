package com.web.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebBrowser {
	
	
	private String univadhisURL = ReadMyProperties.getMyProperty("URL");
	private WebDriver _webDriver;

	public WebBrowser(BrowserType browserName) {
		WebDriverManager.chromedriver().setup();
		switch (browserName) {
		case CHROME:
			_webDriver = new ChromeDriver();
			break;
		case FIREFOX:
			_webDriver = new FirefoxDriver();
			break;
		case IE:
			_webDriver = new InternetExplorerDriver();
			break;
		case EDGE:
			_webDriver = new EdgeDriver();
			break;
		}
	}
	
	public void navigateToURL() {
		_webDriver.navigate().to(univadhisURL);
		_webDriver.manage().window().maximize();
	}
	
	public WebElement getElement(String selector,ElementSelectorType selectorType) {
		WebElement webElement = null;
		switch(selectorType) {
		case ID:
			webElement = getElementByID(selector);
			break;
		case Name:
			webElement = getElementByName(selector);
			break;
		case CssSelector:
			webElement = getElementByCss(selector);
			break;
		case Xpath:
			webElement = getElementByXpath(selector);
			break;
		}
		return webElement;
	}
	private WebElement getElementByID(String selector) {
		return _webDriver.findElement(By.id(selector));
	}
	private WebElement getElementByName(String selector) {
		return _webDriver.findElement(By.name(selector));
	}
	private WebElement getElementByCss(String selector) {
		return _webDriver.findElement(By.cssSelector(selector));
	}
	private WebElement getElementByXpath(String selector) {
		return _webDriver.findElement(By.xpath(selector));
	}
	
	public boolean waitForElement(String selector, ElementSelectorType selectorType) {
		boolean waitForElement = false;
		switch(selectorType) {
		case ID:
			waitForElement = waitForElementByID(selector);
			break;
		case Name:
			waitForElement = waitForElementByName(selector);
			break;
		case CssSelector:
			waitForElement = waitForElementByCss(selector);
			break;
		case Xpath:
			waitForElement = waitForElementByXpath(selector);
			break;
		}
		return waitForElement;
	}
	private boolean waitForElementByID(String selector) {
		WebDriverWait wait = new WebDriverWait(_webDriver, 30000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(selector))).isDisplayed();
	}
	private boolean waitForElementByName(String selector) {
		WebDriverWait wait = new WebDriverWait(_webDriver, 30000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(selector))).isDisplayed();
	}
	private boolean waitForElementByCss(String selector) {
		WebDriverWait wait = new WebDriverWait(_webDriver, 30000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector))).isDisplayed();
	}
	private boolean waitForElementByXpath(String selector) {
		WebDriverWait wait = new WebDriverWait(_webDriver, 30000);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selector))).isDisplayed();
	}
	
	public void closeBrowser() {
		_webDriver.close();
	}
	public void quitBrowser() {
		_webDriver.quit();
	}
}
